# This is the main engine for SIESTA tests.
# We should probably leave only the "simple" tests as the default
# without their output verification. This can be done by choosing
# the appropriate tests when making the cmake targets (maybe via labels?)

file(MAKE_DIRECTORY "${PROJECT_BINARY_DIR}/install_tests")
set(_TEST_PSEUDO_DIR "${CMAKE_CURRENT_SOURCE_DIR}/Pseudos")
set(_TEST_GLOB_DIR "${CMAKE_CURRENT_BINARY_DIR}")

file(COPY out_digest.awk DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
file(COPY cmp_digest.sh DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")

function(siesta_parseout)
  # Parse output files.
  set(options SERIAL)
  set(oneValueArgs NAME OUTDIR OUTFILE PARENT)
  set(multiValueArgs EXTRA_FILES LABELS)
  cmake_parse_arguments(_spout "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if(DEFINED _spout_UNPARSED_ARGUMENTS)
    if(NOT DEFINED _spout_NAME)
      # get name from the first argument
      list(POP_BACK _spout_UNPARSED_ARGUMENTS _spout_NAME)
    endif()
    list(LENGTH _spout_UNPARSED_ARGUMENTS _spout_len)
    if(_spout_len GREATER 0)
      message(FATAL_ERROR "Unparsed arguments in siesta_test test=${_spout_NAME}"
        "Arguments are:\n" "  ${_spout_UNPARSED_ARGUMENTS}")
    endif()
  endif()

  set(_spout_REFDIR "${CMAKE_CURRENT_SOURCE_DIR}/Reference")

  add_test(
    NAME "verify-${_spout_NAME}"
    COMMAND bash ${_TEST_GLOB_DIR}/cmp_digest.sh ${_spout_OUTDIR}/${_spout_OUTFILE}.out ${_spout_REFDIR}/${_spout_NAME}.out
  )
  set_tests_properties(verify-${_spout_NAME}
    PROPERTIES
      ENVIRONMENT "SCRIPTS_DIR=${_TEST_GLOB_DIR}"
      LABELS "${_spout_LABELS}"
      DEPENDS "${_spout_PARENT}")
  # Possible way to enforce dependency: FIXTURES_REQUIRED "${_spout_OUTFILE}-dep"
endfunction()

#  Function to set up tests
#
# It accepts a few arguments
# NAME : directory of test (optional, an unnamed argument will be accepted as NAME)
# PSEUDO_DIR : files to be copied in as pseudos
# MPI_NPROC : number of MPI processors
# OMP_NPROC : number of OpenMP processors
# EXTRA_FILES : extra files to be copied in to the run directory
# SERIAL : to skip MPI (regardless of other arguments)
# This function uses these external variables:
#  WITH_MPI
#  WITH_OPENMP
#  num_ranks
### SUBTEST
function(siesta_subtest)
  set(options SERIAL)
  set(oneValueArgs NAME MPI_NPROC PSEUDO_DIR NAME_ALTER TEST_EXE)
  set(multiValueArgs EXTRA_FILES LABELS)
  cmake_parse_arguments(_stest "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  # Check for bad arguments
  # A single unparsed-argument will be the equivalent of NAME
  # in case it has not been passed.
  if(DEFINED _stest_UNPARSED_ARGUMENTS)
    if(NOT DEFINED _stest_NAME)
      # get name from the first argument
      list(POP_BACK _stest_UNPARSED_ARGUMENTS _stest_NAME)
    endif()
    list(LENGTH _stest_UNPARSED_ARGUMENTS _stest_len)
    if(_stest_len GREATER 0)
      message(FATAL_ERROR "Unparsed arguments in siesta_test test=${_stest_NAME}"
        "Arguments are:\n"
        "  ${_stest_UNPARSED_ARGUMENTS}")
    endif()
  endif()

  # Keyword only options
  # In this case serial may be used to overwrite MPI and OpenMP tests
  if(NOT DEFINED _stest_SERIAL)
    set(_stest_SERIAL FALSE)
  endif()

  # Name of test *must* be defined
  if(NOT DEFINED _stest_NAME)
    message(FATAL_ERROR "siesta_test missing test directory argument: NAME")
  endif()

  message(VERBOSE "Adding new test ${_stest_NAME}")
  list(APPEND CMAKE_MESSAGE_INDENT "  ")

  # Check for number of ranks. It will be reduced to {num_ranks} if too high.
  if(DEFINED _stest_MPI_NPROC)
    if(WITH_MPI)
      if(_stest_MPI_NPROC GREATER num_ranks)
        message(WARNING "Reducing number of MPI processors in test ${_stest_NAME} to ${num_ranks}<${_stest_MPI_NPROC}")
        set(_stest_MPI_NPROC ${num_ranks})
      endif()
    endif()
    message(VERBOSE "using MPI with ${_stest_MPI_NPROC} processors")
  else()
    # set default value
    set(_stest_MPI_NPROC ${num_ranks})
  endif()

  # Check for OpenMP
  # It will understand this variable as the maximum value
  if(DEFINED ENV{OMP_NUM_THREADS})
    # set the OMP_NPROC to this value if undefined
    if(DEFINED _stest_OMP_NPROC)
      if(_stest_OMP_NPROC GREATER ENV{OMP_NUM_THREADS})
        message(WARNING "Reducing number of OpenMP ranks in test ${_stest_NAME} to ${ENV{OMP_NUM_THREADS}}<${_stest_OMP_NPROC}")
        set(_stest_OMP_NPROC ${ENV{OMP_NUM_THREADS}})
      endif()
    else()
      set(_stest_OMP_NPROC 1)
    endif()
    message(VERBOSE "using OpenMP with ${_stest_OMP_NPROC} processors")
  else()
    # set default value of 1
    set(_stest_OMP_NPROC 1)
  endif()

  if(NOT DEFINED _stest_PSEUDO_DIR)
     set(_stest_PSEUDO_DIR "${_TEST_PSEUDO_DIR}")
  endif()
  message(VERBOSE "test(${_stest_NAME}) will be using pseudos from: ${_stest_PSEUDO_DIR}")

  # Build up the variables
  # First set the test name
  if(DEFINED _stest_NAME_ALTER)
    set(_stest_test ${_stest_NAME_ALTER})
  else()
    set(_stest_test ${_stest_NAME})
  endif()
  set(_stest_env "SIESTA_PS_PATH=${_stest_PSEUDO_DIR}") # env-vars to add
  set(_stest_pre "")
  set(_stest_post "")
  if( WITH_MPI AND (NOT _stest_SERIAL) )
    set(_stest_pre
      ${MPIEXEC_NUMPROC_FLAG} ${_stest_MPI_NPROC} ${MPIEXEC_PREFLAGS}
      )
    set(_stest_post
      "${MPIEXEC_POSTFLAGS}"
      )
    set(_stest_test "${_stest_test}_mpi_np${_stest_MPI_NPROC}")
  endif()

  if( WITH_OPENMP AND (NOT _stest_SERIAL) )
    list(APPEND _stest_env "OMP_NUM_THREADS=${_stest_OMP_NPROC}")
    set(_stest_test "${_stest_test}_omp_np${_stest_OMP_NPROC}")
  endif()

  # Create the working directory
  # Since we might have multiple tests for the same directory,
  # we will need to parse the options before doing anything
  set(_stest_tdir "${CMAKE_CURRENT_BINARY_DIR}/${_stest_test}")
  set(_stest_wdir "${CMAKE_CURRENT_BINARY_DIR}/${_stest_test}/work")
  get_filename_component(_stest_parent_dir "${CMAKE_CURRENT_BINARY_DIR}" NAME)

  # Now we can prepare the test directory:
  file(MAKE_DIRECTORY "${_stest_tdir}")
  file(MAKE_DIRECTORY "${_stest_wdir}")
  file(GLOB _stest_FDF_FILES "*.fdf")
  file(COPY ${_stest_FDF_FILES} DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")

  # Copy in extra files
  if(DEFINED _stest_EXTRA_FILES)
    message(VERBOSE "test(${_stest_NAME}) copying extra files: ${_stest_EXTRA_FILES}")
    foreach(_extra IN LISTS _stest_EXTRA_FILES)
      file(COPY "${_extra}" DESTINATION "${_stest_wdir}")
    endforeach()
  endif()

  # Dirty fix for transiesta test.
  if(DEFINED _stest_TEST_EXE)
    set(_stest_EXE ${_stest_TEST_EXE})
  else()
    set(_stest_EXE ${PROJECT_NAME}-siesta)
  endif()

  set(_stest_FULL_NAME "${_stest_EXE}-${_stest_parent_dir}-${_stest_test}")
  # Now add the test
  if( WITH_MPI AND (NOT _stest_SERIAL) )
    add_test(
      NAME "${_stest_FULL_NAME}"
      COMMAND ${MPIEXEC_EXECUTABLE} ${_stest_pre} $<TARGET_FILE:${_stest_EXE}> -out ${_stest_test}.out ../../${_stest_NAME}.fdf ${_stest_post}
      WORKING_DIRECTORY "${_stest_wdir}"  )
  else()
    add_test(
      NAME "${_stest_FULL_NAME}"
      COMMAND $<TARGET_FILE:${_stest_EXE}> -out ${_stest_test}.out ../../${_stest_NAME}.fdf ${_stest_post}
      WORKING_DIRECTORY "${_stest_wdir}"  )
  endif()
  # Add environment variables to the test
  set_tests_properties(${_stest_FULL_NAME}
    PROPERTIES
      ENVIRONMENT "${_stest_env}"
      LABELS "${_stest_LABELS}")
  # Possible way to enforce dependency: FIXTURES_SETUP "${_stest_test}-dep"
  list(POP_BACK CMAKE_MESSAGE_INDENT)

  # Add output verification
  if(DEFINED _stest_NAME_ALTER)
    siesta_parseout(${_stest_NAME_ALTER}
                    PARENT ${_stest_FULL_NAME} OUTFILE ${_stest_test}
                    OUTDIR ${_stest_wdir} LABELS "${_stest_LABELS}")
  else()
    siesta_parseout(${_stest_NAME}
                    PARENT ${_stest_FULL_NAME} OUTFILE ${_stest_test}
                    OUTDIR ${_stest_wdir} LABELS "${_stest_LABELS}")
  endif()

endfunction()

add_subdirectory(00.BasisSets)
add_subdirectory(01.PseudoPotentials)
add_subdirectory(02.SpinPolarization)
add_subdirectory(03.SpinOrbit)
add_subdirectory(04.SCFMixing)
add_subdirectory(05.Bands)
add_subdirectory(06.DensityOfStates)
add_subdirectory(07.ForceConstants)
add_subdirectory(08.GeometryOptimization)
add_subdirectory(09.MolecularDynamics)
add_subdirectory(10.Functionals)
add_subdirectory(11.ElectronicProperties)
add_subdirectory(12.Solvers)
add_subdirectory(13.MiscOptions)
add_subdirectory(14.FileIO)
add_subdirectory(15.TDDFT)
add_subdirectory(16.TranSiesta)
if(WITH_WANNIER90)
  add_subdirectory(17.Wannier90)
endif()

add_subdirectory(Dependency_Tests)
